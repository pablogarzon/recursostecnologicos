package com.ppai.rescursostecologicos.ReservaTurnos;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;

import com.ppai.rescursostecologicos.ReservaTurnos.dto.CalendarDTO;
import com.ppai.rescursostecologicos.ReservaTurnos.dto.ConfirmacionDTO;
import com.ppai.rescursostecologicos.ReservaTurnos.dto.EventCalendarDTO;
import com.ppai.rescursostecologicos.ReservaTurnos.dto.RecursoTecnologicoDTO;
import com.ppai.rescursostecologicos.ReservaTurnos.dto.TableRTDTO;
import com.ppai.rescursostecologicos.dominio.Cientifico;
import com.ppai.rescursostecologicos.dominio.Estado;
import com.ppai.rescursostecologicos.dominio.RecursoTecnologico;
import com.ppai.rescursostecologicos.dominio.Sesion;
import com.ppai.rescursostecologicos.dominio.TipoRecursoTecnologico;
import com.ppai.rescursostecologicos.dominio.Turno;
import com.ppai.rescursostecologicos.services.EstadosService;
import com.ppai.rescursostecologicos.services.InterfazMail;
import com.ppai.rescursostecologicos.services.RecursoTecnologicoService;
import com.ppai.rescursostecologicos.services.TiposRecursoTecnologicoService;

@Controller
@RequestMapping("/")
public class GestorReservaTurno {

	private final TiposRecursoTecnologicoService tiposRecursoTecnologico;
	private final RecursoTecnologicoService recursosTecnologicos;
	private final EstadosService estados;
	private final Sesion actual;
	private final InterfazMail mail;

	@Autowired
	public GestorReservaTurno(TiposRecursoTecnologicoService tiposRecursoTecnologico,
			RecursoTecnologicoService recursosTecnologicos, Sesion sesion, EstadosService estados, InterfazMail mail) {
		this.tiposRecursoTecnologico = tiposRecursoTecnologico;
		this.recursosTecnologicos = recursosTecnologicos;
		this.actual = sesion;
		this.estados = estados;
		this.mail = mail;
	}

	/**
	 * Habilita la pantalla a usar, spring internamente busca el template en la carpeta templates, en este caso
	 * sería: src/main/resources/templates/PantallaReservaTurno.html
	 * @return nombre de la plantilla a usar
	 */
	@GetMapping
	public String nuevaReservaTurno() {
		return "PantallaReservaTurno";
	}

	/** 
	 * Obtener la lista de recursos Tecnológicos
	 * @return listado de TipoRecursoTecnologico
	 */
	@GetMapping(path = "/buscarTipoRecursos", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<TipoRecursoTecnologico> buscarTipoRecursos() {
		return this.tiposRecursoTecnologico.mostrarTipoRecursos();
	}

	/**
	 * busca los recursos tecnologicos que sean del tipo seleccionado por el usuario
	 * @param idTipoRecurso
	 * @return un listado de objetos que representa los recursos tecnologicos agrupados por CI
	 */
	@GetMapping(path = "/recursosXTipoSeleccionado", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<TableRTDTO> tipoRTSeleccionado(@RequestParam("idTipoRecurso") int idTipoRecurso) {
		return buscarRecursosXTipoSeleccionado(idTipoRecurso);
	}

	/**
	 * filtra los recursos tecnologicos que son del tipo seleccionado y que esten activos
	 * @param idTipoRecurso
	 * @return un listado de objetos que representa los recursos tecnologicos
	 */
	private List<TableRTDTO> buscarRecursosXTipoSeleccionado(int idTipoRecurso) {

		List<RecursoTecnologicoDTO> recursosTecnologicosActivos = new ArrayList<>();

		for (RecursoTecnologico recursoTecnologico : this.recursosTecnologicos.obtenerRecursosTecnologicos()) {
			if (recursoTecnologico.esTipoRecursoSeleccionado(idTipoRecurso)) {
				if (recursoTecnologico.esActivo()) {
					var datos = recursoTecnologico.mostrarDatos();
					recursosTecnologicosActivos.add(RecursoTecnologicoDTO.crearDatos(datos));
				}
			}
		}

		return agruparPorCI(recursosTecnologicosActivos);
	}

	/**
	 * agrupa los recuros tecnológicos por CI
	 * @param recursos -> recursos que son del tipo seleccionado y que estan actiovs
	 * @return un listado de objetos que representa los recursos tecnologicos agrupados por CI
	 */
	private List<TableRTDTO> agruparPorCI(List<RecursoTecnologicoDTO> recursos) {
		List<TableRTDTO> resultados = new ArrayList<>();

		var agrupados = recursos.stream()
				.collect(Collectors.groupingBy(RecursoTecnologicoDTO::getCi, Collectors.toList()));

		for (Map.Entry<String, List<RecursoTecnologicoDTO>> set : agrupados.entrySet()) {
			resultados.add(new TableRTDTO(set.getKey(), set.getValue()));
		}

		return resultados;
	}

	/**
	 * obtiene los turnos disponibles para el recurso seleccionado
	 * @param rt -> recurso tecnologico seleccionado
	 * @return un listado de turnos en un formato que se ajusta al aceptado por el componente calendario en pantalla
	 */
	@GetMapping(path = "/obtenerTurnos")
	@ResponseBody
	public List<CalendarDTO> RTSeleccionado(@RequestParam("id") int rt) {
		RecursoTecnologico seleccionado = recursosTecnologicos.getById(rt);
		Cientifico logueado = buscarCientifico();
		return verificarCentrodeInvestigacionCientificoLogueado(seleccionado, logueado);
	}

	private Cientifico buscarCientifico() {
		return actual.obtenerCientificoLogueado();
	}

	/**
	 * obtiene los turnos disponibles para el recurso seleccionado verificando que el cientifico pertenezca al CI 
	 * @param seleccionado -> recurso tecnologico seleccionado
	 * @param logueado -> cientifico logueado
	 * @return un listado de turnos en un formato que se ajusta al aceptado por el componente calendario en pantalla
	 */
	private List<CalendarDTO> verificarCentrodeInvestigacionCientificoLogueado(
			RecursoTecnologico seleccionado, Cientifico logueado) {
		if (seleccionado.perteneceAMiCentro(logueado)) {
			return obtenerTurnosReservablesRTSeleccionado(seleccionado);
		} else {
			throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
		}
	}

	private List<CalendarDTO> obtenerTurnosReservablesRTSeleccionado(RecursoTecnologico seleccionado) {
		LocalDate fechaActual = buscarFechaActual();
		List<Turno> turnosDisponibles = determinarDisponibilidadTurnos(seleccionado, fechaActual);
		return agruparYOrdenarTurnos(turnosDisponibles);
	}

	private List<Turno> determinarDisponibilidadTurnos(RecursoTecnologico seleccionado, LocalDate fechaActual) {
		return seleccionado.misTurnosDisponibles(fechaActual);
	}

	/**
	 * agrupa y ordena los turnos, determina el color de estos turnos y si el día esta completo 
	 * @param turnosDisponibles
	 * @return un listado de turnos en un formato que se ajusta al aceptado por el componente calendario en pantalla
	 */
	private List<CalendarDTO> agruparYOrdenarTurnos(List<Turno> turnosDisponibles) {
		var result = new ArrayList<CalendarDTO>();
		
		var agrupados = turnosDisponibles.stream().sorted((t1, t2) -> t1.getFechaHoraInicio().compareTo(t2.getFechaHoraInicio()))
				.collect(Collectors.groupingBy(Turno::getDiaSemana, Collectors.toList()));
		
		for (var set : agrupados.entrySet()) {
			List<EventCalendarDTO> eventos = set.getValue().stream()
					.map(t -> {
						String estado = t.getEstado().getNombre();
						String color = null;
						if(estado.equals("DISPONIBLE")) {
							color = "azul";
						} else if(estado.equals("PENDIENTE")) {
							color = "gris";
						} else {
							color = "rojo";
						}
						return EventCalendarDTO.crearCalendario(t, estado, color);
					})
					.collect(Collectors.toList());
			
			String colorDia = "rojo"; 
			if(eventos.stream().anyMatch(turno -> turno.getColor().equals("azul"))) {
				colorDia = "azul";
			}
			
			result.add(new CalendarDTO(set.getKey(), colorDia, eventos));
		}
		
		return result;
	}

	private LocalDate buscarFechaActual() {
		return LocalDateTime.now().toLocalDate();
	}

	@GetMapping(path = "formasDeNotificacion")
	public List<String> turnoSeleccionado(Turno turno) {
		return presentarFormasDeNotificacion();
	}

	private List<String> presentarFormasDeNotificacion() {
		var notificaciones = new ArrayList<String>();
		notificaciones.add("Por Mail");
		notificaciones.add("Por Whatsapp");
		return notificaciones;
	}

	/**
	 * toma la confirmación de reserva del turno
	 * @param data -> objeto que contiene el turno seleccionado y el id del recurso seleccionado
	 * @return HTTP OK si esta todo bien
	 */
	@PostMapping
	public ResponseEntity<?> tomarConfirmacion(@RequestBody ConfirmacionDTO data) {
		RecursoTecnologico seleccionado = recursosTecnologicos.getById(data.getRecursoSeleccionado());
		Turno turnoSeleccionado = seleccionado.getMisTurnos().stream()
				.filter(x -> x.getNroTurno() == data.getTurnoSeleccionado())
				.findFirst()
				.get();
		generarReservaRTSeleccionado(turnoSeleccionado);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	/**
	 * busca el estado reservado y registra la reserva
	 * @param turnoSeleccionado
	 */
	private void generarReservaRTSeleccionado(Turno turnoSeleccionado) {
		Estado estadoReservado = null;
		for (Estado estado : estados.obtenerEstados()) {
			if (estado.esDeAmbitoTurno()) {
				if (estado.esReservado()) {
					estadoReservado = estado;
				}
			}
		}

		registrarReserva(turnoSeleccionado, estadoReservado);
	}

	private void registrarReserva(Turno turnoSeleccionado, Estado estadoReservado) {
		LocalDate fechaActual = buscarFechaActual();
		turnoSeleccionado.reservar(fechaActual);
		notificarMail();
	}

	private void notificarMail() {
		mail.notificar();
	}
}
