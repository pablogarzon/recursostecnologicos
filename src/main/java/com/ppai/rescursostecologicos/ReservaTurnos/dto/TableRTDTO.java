package com.ppai.rescursostecologicos.ReservaTurnos.dto;

import java.util.List;

public class TableRTDTO {
	private String label;
	private List<RecursoTecnologicoDTO> children;
	
	public TableRTDTO(String label, List<RecursoTecnologicoDTO> children) {
		super();
		this.label = label;
		this.children = children;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public List<RecursoTecnologicoDTO> getChildren() {
		return children;
	}
	public void setChildren(List<RecursoTecnologicoDTO> children) {
		this.children = children;
	}
}
