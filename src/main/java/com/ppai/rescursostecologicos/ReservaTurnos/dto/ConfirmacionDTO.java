package com.ppai.rescursostecologicos.ReservaTurnos.dto;

//
public class ConfirmacionDTO {
	private int recursoSeleccionado;
	private int turnoSeleccionado;
	
	public ConfirmacionDTO() {
		
	}
	
	public ConfirmacionDTO(int recursoSeleccionado, int turnoSeleccionado) {
		super();
		this.recursoSeleccionado = recursoSeleccionado;
		this.turnoSeleccionado = turnoSeleccionado;
	}

	public int getRecursoSeleccionado() {
		return recursoSeleccionado;
	}

	public void setRecursoSeleccionado(int recursoSeleccionado) {
		this.recursoSeleccionado = recursoSeleccionado;
	}

	public int getTurnoSeleccionado() {
		return turnoSeleccionado;
	}

	public void setTurnoSeleccionado(int turnoSeleccionado) {
		this.turnoSeleccionado = turnoSeleccionado;
	}
}
