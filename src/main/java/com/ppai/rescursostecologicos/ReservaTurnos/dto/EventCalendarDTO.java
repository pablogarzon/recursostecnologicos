package com.ppai.rescursostecologicos.ReservaTurnos.dto;

import java.time.format.DateTimeFormatter;

import com.ppai.rescursostecologicos.dominio.Turno;

public class EventCalendarDTO {
	private String start;
	private String end;
	private String title;
	private String content;
	private String color;
	
	private static final DateTimeFormatter formatterLocalDateTime = 
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	
	public EventCalendarDTO(String start, String end, String title, String content, String color) {
		super();
		this.start = start;
		this.end = end;
		this.title = title;
		this.content = content;
		this.color = color;
	}
	
	public static EventCalendarDTO crearCalendario(Turno t, String estado, String color) {
		return new EventCalendarDTO(
				formatterLocalDateTime.format(t.getFechaHoraInicio()), 
				formatterLocalDateTime.format(t.getFechaHoraFin()),
				estado + "-" + t.getNroTurno(), "", color);
	}
	
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
}
