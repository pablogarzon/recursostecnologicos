package com.ppai.rescursostecologicos.ReservaTurnos.dto;

import java.util.List;

public class CalendarDTO {
	private String fecha;
	private String color;
	private List<EventCalendarDTO> turnos;
	
	public CalendarDTO(String fecha, String color, List<EventCalendarDTO> turnos) {
		super();
		this.fecha = fecha;
		this.color = color;
		this.turnos = turnos;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public List<EventCalendarDTO> getTurnos() {
		return turnos;
	}

	public void setTurnos(List<EventCalendarDTO> turnos) {
		this.turnos = turnos;
	}
}
