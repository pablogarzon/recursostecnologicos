package com.ppai.rescursostecologicos.ReservaTurnos.dto;

import java.util.Map;

public class RecursoTecnologicoDTO {
	private int idRT;
	private String estado;
	private String ci;
	private String marca;
	private String modelo;
	
	public RecursoTecnologicoDTO(int idRT, String estado, String ci, String marca, String modelo) {
		super();
		this.idRT = idRT;
		this.estado = estado;
		this.ci = ci;
		this.marca = marca;
		this.modelo = modelo;
	}
	
	
	public static RecursoTecnologicoDTO crearDatos(Map<String, String> datos) {
		return new RecursoTecnologicoDTO(
				Integer.valueOf(datos.get("numero")), 
				datos.get("estado"),
				datos.get("ci"), 
				datos.get("marca"), 
				datos.get("modelo"));
	}


	public int getIdRT() {
		return idRT;
	}
	public void setIdRT(int idRT) {
		this.idRT = idRT;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCi() {
		return ci;
	}
	public void setCi(String ci) {
		this.ci = ci;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
}
