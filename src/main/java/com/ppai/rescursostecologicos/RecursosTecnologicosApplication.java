package com.ppai.rescursostecologicos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class RecursosTecnologicosApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecursosTecnologicosApplication.class, args);
	}

}
