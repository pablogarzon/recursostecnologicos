package com.ppai.rescursostecologicos.dominio;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

@Entity
public class RecursoTecnologico {
	
	@Id
	private int numeroRT;
	
	@OneToOne(fetch = FetchType.EAGER)
	private CentroInvestigacion CI;
	
	@OneToOne(fetch = FetchType.EAGER)
	private Marca marca;
	
	@OneToOne(fetch = FetchType.EAGER)
	private TipoRecursoTecnologico tipo;
	
	@OneToMany(fetch = FetchType.EAGER)
	private List<CambioEstadoRT> cambiosDeEstado;

	@OneToMany(fetch = FetchType.EAGER)
	private List<Turno> misTurnos;
	
	@Transient
	private Estado actual;
	
	public RecursoTecnologico() {
	}
	
	public RecursoTecnologico(int numeroRT, CentroInvestigacion cI, Marca marca, TipoRecursoTecnologico tipo,
			List<CambioEstadoRT> cambiosDeEstado, List<Turno> misTurnos) {
		super();
		this.numeroRT = numeroRT;
		CI = cI;
		this.marca = marca;
		this.tipo = tipo;
		this.cambiosDeEstado = cambiosDeEstado;
		this.misTurnos = misTurnos;
	}

	public boolean esTipoRecursoSeleccionado(int tipoRecurso) {
		return this.tipo.getId() == tipoRecurso;
	}

	public boolean esActivo() {
		
		for (CambioEstadoRT cambioEstadoRT : cambiosDeEstado) {
			if(cambioEstadoRT.esActual()) {
				if(cambioEstadoRT.esEstadoActualReservable()) {
					actual = cambioEstadoRT.getEstado();
					return true;
				} else {
					return false;
				}
			}
		}
		
		return false;
	}
	
	public boolean perteneceAMiCentro(Cientifico logueado) {
		return CI.esCientificoActivo(logueado);
	}

	public List<Turno> misTurnosDisponibles(LocalDate fechaActual) {
		List<Turno> disponibles = new ArrayList<>();
		
		for (Turno turno : misTurnos) {
			if(turno.esPosteriorAFecha(fechaActual)) {
				turno.mostrarTurno();
				disponibles.add(turno);
			}
		}
		
		return disponibles;
	}

	public CentroInvestigacion getCI() {
		return CI;
	}

	public void setCI(CentroInvestigacion cI) {
		CI = cI;
	}
	
	public List<CambioEstadoRT> getCambiosDeEstado() {
		return cambiosDeEstado;
	}

	public void setCambiosDeEstado(List<CambioEstadoRT> cambiosDeEstado) {
		this.cambiosDeEstado = cambiosDeEstado;
	}

	public List<Turno> getMisTurnos() {
		return misTurnos;
	}

	public void setMisTurnos(List<Turno> misTurnos) {
		this.misTurnos = misTurnos;
	}

	public TipoRecursoTecnologico getTipo() {
		return tipo;
	}

	public void setTipo(TipoRecursoTecnologico tipo) {
		this.tipo = tipo;
	}

	public int getNumeroRT() {
		return numeroRT;
	}

	public void setNumeroRT(int numeroRT) {
		this.numeroRT = numeroRT;
	}

	public Map<String, String> mostrarDatos() {
		var datos = new HashMap<String, String>();
		
		datos.put("numero", String.valueOf(this.getNumeroRT()));
		datos.put("estado", this.actual.getNombre());
		datos.put("ci", this.CI.getNombre());
		datos.put("marca", this.marca.getNombre());
		datos.put("modelo", this.marca.mostrarModelo());
		
		return datos;
	}
}
