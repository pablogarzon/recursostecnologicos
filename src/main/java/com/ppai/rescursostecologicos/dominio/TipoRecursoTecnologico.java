package com.ppai.rescursostecologicos.dominio;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TipoRecursoTecnologico {
	
	@Id
	private int id;
	
	private String nombre;

	public TipoRecursoTecnologico() {
	}
	
	public TipoRecursoTecnologico(int id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
