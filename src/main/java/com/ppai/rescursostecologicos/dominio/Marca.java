package com.ppai.rescursostecologicos.dominio;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Marca {
	
	@Id
	private String nombre;
	
	@OneToOne(fetch = FetchType.EAGER)
	private Modelo modelo;
	
	public Marca() {
	}
	
	public Marca(String nombre, Modelo modelo) {
		super();
		this.nombre = nombre;
		this.modelo = modelo;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Modelo getModelo() {
		return modelo;
	}
	public void setModelo(Modelo modelo) {
		this.modelo = modelo;
	}

	public String mostrarModelo() {
		return modelo.getNombre();
	}
}
