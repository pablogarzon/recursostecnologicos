package com.ppai.rescursostecologicos.dominio.estados.turno;

import java.time.LocalDate;
import java.util.List;

import com.ppai.rescursostecologicos.dominio.CambioEstadoTurno;
import com.ppai.rescursostecologicos.dominio.Estado;
import com.ppai.rescursostecologicos.dominio.Turno;

public class Disponible extends Estado {

	public Disponible() {
		this.setNombre(ESTADOS.DISPONIBLE.name());
		this.setAmbito(AMBITOS.TURNO.name());
	}
	
	@Override
	public void reservar(Turno turno, LocalDate fechaActual) {
		List<CambioEstadoTurno> cambios = turno.getCambioEstado();
		finalizarCambioEstado(cambios, fechaActual);
		Estado estado = this.crearProximoEstado();
		CambioEstadoTurno cambioEstado = this.crearProximoCambioEstado(fechaActual, estado);
		turno.agregarCambioEstado(cambioEstado);
		turno.setEstado(estado);
	}

	private void finalizarCambioEstado(List<CambioEstadoTurno> cambioEstado, LocalDate fechaActual) {
		for (CambioEstadoTurno cambioEstadoTurno : cambioEstado) {
			if(cambioEstadoTurno.esActual()) {
				cambioEstadoTurno.setFechaHasta(fechaActual);
			}
		}
	}

	@Override
	public void registarUso() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registarFinUso() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cancelar() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void rechazar() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Estado crearProximoEstado() {
		return new PendienteDeConfirmacion();
	}

	@Override
	public CambioEstadoTurno crearProximoCambioEstado(LocalDate fechaActual, Estado estado) {
		return new CambioEstadoTurno(estado, fechaActual, null);
		
	}

}
