package com.ppai.rescursostecologicos.dominio.estados.turno;

import java.time.LocalDate;

import com.ppai.rescursostecologicos.dominio.CambioEstadoTurno;
import com.ppai.rescursostecologicos.dominio.Estado;
import com.ppai.rescursostecologicos.dominio.Turno;

public class Finalizado extends Estado {

	public Finalizado() {
		this.setNombre(ESTADOS.FINALIZADO.name());
		this.setAmbito(AMBITOS.TURNO.name());
	}
	
	@Override
	public void reservar(Turno turno, LocalDate fechaActual) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registarUso() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void registarFinUso() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void cancelar() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void rechazar() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Estado crearProximoEstado() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CambioEstadoTurno crearProximoCambioEstado(LocalDate fechaActual, Estado estado) {
		// TODO Auto-generated method stub
		return null;
	}

}
