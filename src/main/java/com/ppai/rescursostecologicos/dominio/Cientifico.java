package com.ppai.rescursostecologicos.dominio;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Cientifico {
	
	@Id
	private int legajo;
	
	private String nombre;
	private String email;
	
	public Cientifico() {
	}
	
	public Cientifico(int legajo, String nombre, String email) {
		super();
		this.legajo = legajo;
		this.nombre = nombre;
		this.email = email;
	}
	
	public int getLegajo() {
		return legajo;
	}
	public void setLegajo(int legajo) {
		this.legajo = legajo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
