package com.ppai.rescursostecologicos.dominio;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class AsignacionCientifico {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@OneToOne(fetch = FetchType.EAGER)
	private Cientifico cientifico;
	
	private LocalDate fechaDesde;
	private LocalDate fechaHasta;
	
	public AsignacionCientifico() {
	
	}

	public AsignacionCientifico(Cientifico cientifico) {
		super();
		this.cientifico = cientifico;
	}
	
	public AsignacionCientifico(int id, Cientifico cientifico, LocalDate fechaDesde, LocalDate fechaHasta) {
		super();
		this.id = id;
		this.cientifico = cientifico;
		this.fechaDesde = fechaDesde;
		this.fechaHasta = fechaHasta;
	}

	public boolean esCientificoActivo(Cientifico logueado) {
		return logueado.getNombre().equals(this.cientifico.getNombre());
	}

	public Cientifico getCientifico() {
		return cientifico;
	}

	public void setCientifico(Cientifico cientifico) {
		this.cientifico = cientifico;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(LocalDate fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public LocalDate getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(LocalDate fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
}
