package com.ppai.rescursostecologicos.dominio;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.ppai.rescursostecologicos.dominio.estados.rt.Reservable;

@Entity
public class CambioEstadoRT {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	//@OneToOne(fetch = FetchType.EAGER)
	@Transient
	private Estado estado = new Reservable();
	
	private LocalDate fechaDesde;
	private LocalDate fechaHasta;
	
	public CambioEstadoRT() {
	}

	public CambioEstadoRT(Estado estado, LocalDate fechaDesde, LocalDate fechaHasta) {
		super();
		this.estado = estado;
		this.fechaDesde = fechaDesde;
		this.fechaHasta = fechaHasta;
	}

	public boolean esActual() {
		return fechaHasta == null;
	}

	public boolean esEstadoActualReservable() {
		return estado.esReservable();
	}

	public LocalDate getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(LocalDate fechaDesde) {
		this.fechaDesde = fechaDesde;
	}
	
	public Estado getEstado() {
		return this.estado;
	}
}
