package com.ppai.rescursostecologicos.dominio;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.ppai.rescursostecologicos.dominio.estados.turno.Disponible;

@Entity
public class CambioEstadoTurno {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	//@OneToOne(fetch = FetchType.EAGER)
	@Transient
	private Estado estado = new Disponible();
	
	private LocalDate fechaDesde;
	private LocalDate fechaHasta;
	
	public CambioEstadoTurno() {
	}
	
	public CambioEstadoTurno(Estado estado, LocalDate fechaDesde, LocalDate fechaHasta) {
		super();
		this.estado = estado;
		this.fechaDesde = fechaDesde;
		this.fechaHasta = fechaHasta;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public LocalDate getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(LocalDate fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public LocalDate getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(LocalDate fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public boolean esActual() {
		return this.fechaHasta == null;
	}
}
