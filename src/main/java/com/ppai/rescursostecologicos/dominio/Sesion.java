package com.ppai.rescursostecologicos.dominio;

public class Sesion {
	
	private Usuario usuario;

	public Sesion(Usuario usuario) {
		super();
		this.usuario = usuario;
	}

	public Cientifico obtenerCientificoLogueado() {
		return usuario.getCientifico(); 
	}

}
