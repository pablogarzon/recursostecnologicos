package com.ppai.rescursostecologicos.dominio;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Usuario {

	private String nombre;
	private String password;
	
	@OneToOne
	private Cientifico cientifico;
	
	public Usuario() {
	}
	
	public Usuario(String nombre, String password, Cientifico cientifico) {
		super();
		this.nombre = nombre;
		this.password = password;
		this.setCientifico(cientifico);
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public Cientifico getCientifico() {
		return cientifico;
	}

	public void setCientifico(Cientifico cientifico) {
		this.cientifico = cientifico;
	}
}
