package com.ppai.rescursostecologicos.dominio;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class CentroInvestigacion {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String nombre;
	
	@OneToMany(fetch = FetchType.EAGER)
	private List<AsignacionCientifico> asignaciones;
	
	public CentroInvestigacion() {
	}

	public CentroInvestigacion(String nombre, List<AsignacionCientifico> asignaciones) {
		super();
		this.nombre = nombre;
		this.asignaciones = asignaciones;
	}
	
	public boolean esCientificoActivo(Cientifico logueado) {
		for (AsignacionCientifico asignacionCientifico : asignaciones) {
			if(asignacionCientifico.esCientificoActivo(logueado)) {
				return true;
			}
		}
		
		return false;
	}

	public List<AsignacionCientifico> getAsignaciones() {
		return asignaciones;
	}

	public void setAsignaciones(List<AsignacionCientifico> asignaciones) {
		this.asignaciones = asignaciones;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}	
}
