package com.ppai.rescursostecologicos.dominio;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class Turno {
	
	@Id
	private int nroTurno;
	
	private LocalDateTime fechaHoraInicio;
	private LocalDateTime fechaHoraFin;
	private String diaSemana;
	private LocalDate fechaGeneracion;
	private LocalDate fecha;
	
	@OneToMany(fetch = FetchType.EAGER)
	private List<CambioEstadoTurno> cambioEstado;
	
	@Transient
	private Estado estado;

	public Turno() {
	}
	
	public Turno(int nroTurno, LocalDateTime fechaHoraInicio, LocalDateTime fechaHoraFin, String diaSemana,
			LocalDate fechaGeneracion, LocalDate fecha, List<CambioEstadoTurno> cambioEstado) {
		super();
		this.setNroTurno(nroTurno);
		this.fechaHoraInicio = fechaHoraInicio;
		this.fechaHoraFin = fechaHoraFin;
		this.diaSemana = diaSemana;
		this.fechaGeneracion = fechaGeneracion;
		this.fecha = fecha;
		this.cambioEstado = cambioEstado;
	}

	public boolean esPosteriorAFecha(LocalDate fechaActual) {
		return fecha.isAfter(fechaActual) || fecha.isEqual(fechaActual);
	}
	
	public boolean mostrarTurno() {
		for (CambioEstadoTurno cambioEstadoTurno : cambioEstado) {
			if(cambioEstadoTurno.esActual()) {
				estado = cambioEstadoTurno.getEstado(); 
				return true;
			}
		}
		return false;
	}	
	
	public void reservar(LocalDate fechaActual) {
		this.estado.reservar(this, fechaActual);
//		for (CambioEstadoTurno cambioEstadoTurno : cambioEstado) {
//			if(cambioEstadoTurno.esActual()) {
//				cambioEstadoTurno.setFechaHasta(fechaActual);
//			}
//		}
//		this.cambioEstado.add(crearNuevoCambioEstado(fechaActual));
	}

//	private CambioEstadoTurno crearNuevoCambioEstado(Estado estadoReservado, LocalDate fechaActual) {
//		return new CambioEstadoTurno(estadoReservado, fechaActual, null);
//	}

	public LocalDateTime getFechaHoraInicio() {
		return fechaHoraInicio;
	}

	public void setFechaHoraInicio(LocalDateTime fechaHoraInicio) {
		this.fechaHoraInicio = fechaHoraInicio;
	}

	public LocalDateTime getFechaHoraFin() {
		return fechaHoraFin;
	}

	public void setFechaHoraFin(LocalDateTime fechaHoraFin) {
		this.fechaHoraFin = fechaHoraFin;
	}

	public String getDiaSemana() {
		return diaSemana;
	}

	public void setDiaSemana(String diaSemana) {
		this.diaSemana = diaSemana;
	}

	public LocalDate getFechaGeneracion() {
		return fechaGeneracion;
	}

	public void setFechaGeneracion(LocalDate fechaGeneracion) {
		this.fechaGeneracion = fechaGeneracion;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public List<CambioEstadoTurno> getCambioEstado() {
		return cambioEstado;
	}

	public void setCambioEstado(List<CambioEstadoTurno> cambioEstado) {
		this.cambioEstado = cambioEstado;
	}
	
	public void agregarCambioEstado(CambioEstadoTurno cambioEstado) {
		this.cambioEstado.add(cambioEstado);
	}

	public int getNroTurno() {
		return nroTurno;
	}

	public void setNroTurno(int nroTurno) {
		this.nroTurno = nroTurno;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

}
