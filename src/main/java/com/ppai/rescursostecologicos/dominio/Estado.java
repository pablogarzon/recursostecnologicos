package com.ppai.rescursostecologicos.dominio;

import java.time.LocalDate;

public abstract class Estado {

	public enum ESTADOS {
		CANCELADO_POR_MANTENIMIENTO, ANULADO_NO_EJECUTADO, FINALIZADO, SIN_USO, EN_USO, PENDIENTE_DE_CONFIRMACION,
		CON_RESERVA_CONFIRMADA, DISPONIBLE, RESERVABLE
	}

	public enum AMBITOS {
		TURNO, RT
	}

	private String ambito;
	private String nombre;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getAmbito() {
		return ambito;
	}

	public void setAmbito(String ambito) {
		this.ambito = ambito;
	}

	public boolean esReservado() {
		return nombre.equals(ESTADOS.PENDIENTE_DE_CONFIRMACION.name());
	}

	public boolean esReservable() {
		return nombre.equals(ESTADOS.RESERVABLE.name());
	}

	public boolean esDeAmbitoTurno() {
		return ambito.equals(AMBITOS.TURNO.name());
	}

	public abstract void reservar(Turno turno, LocalDate fechaActual);

	public abstract void registarUso();

	public abstract void registarFinUso();

	public abstract void cancelar();

	public abstract void rechazar();

	public abstract Estado crearProximoEstado();

	public abstract CambioEstadoTurno crearProximoCambioEstado(LocalDate fechaActual, Estado estado);
}
