package com.ppai.rescursostecologicos.dominio;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Modelo {
	
	@Id
	private String nombre;

	public Modelo() {
	}
	
	public Modelo(String nombre) {
		super();
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
