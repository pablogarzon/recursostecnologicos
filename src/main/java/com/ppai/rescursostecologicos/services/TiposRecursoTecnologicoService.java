package com.ppai.rescursostecologicos.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppai.rescursostecologicos.dominio.TipoRecursoTecnologico;

@Service
public class TiposRecursoTecnologicoService {
	
	List<TipoRecursoTecnologico> tipos;
	
	@Autowired
	public TiposRecursoTecnologicoService(List<TipoRecursoTecnologico> tipos) {
		this.tipos = tipos;
	}

	public List<TipoRecursoTecnologico> mostrarTipoRecursos() {		
		return tipos;
	}

}
