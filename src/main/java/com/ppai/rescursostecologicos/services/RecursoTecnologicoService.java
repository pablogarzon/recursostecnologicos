package com.ppai.rescursostecologicos.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ppai.rescursostecologicos.dominio.RecursoTecnologico;

@Service
public class RecursoTecnologicoService {
	
	List<RecursoTecnologico> recursos;
	
	@Autowired
	public RecursoTecnologicoService(List<RecursoTecnologico> recursos) {
		this.recursos = recursos;
	}

	public RecursoTecnologico getById(int rt) {
		return recursos.stream()
				.filter(recursoTecnologico -> recursoTecnologico.getNumeroRT() == rt)
				.findFirst()
				.orElse(null);
	}

	public List<RecursoTecnologico> obtenerRecursosTecnologicos() {
		return recursos;
	}

}
