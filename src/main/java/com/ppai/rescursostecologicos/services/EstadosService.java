package com.ppai.rescursostecologicos.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.ppai.rescursostecologicos.dominio.Estado;
import com.ppai.rescursostecologicos.dominio.estados.turno.AnuladoNoEjecutado;
import com.ppai.rescursostecologicos.dominio.estados.turno.CanceladoPorMantenimiento;
import com.ppai.rescursostecologicos.dominio.estados.turno.ConReservaConfirmada;
import com.ppai.rescursostecologicos.dominio.estados.turno.Disponible;
import com.ppai.rescursostecologicos.dominio.estados.turno.EnUso;
import com.ppai.rescursostecologicos.dominio.estados.turno.Finalizado;
import com.ppai.rescursostecologicos.dominio.estados.turno.PendienteDeConfirmacion;

@Service
public class EstadosService {

	public List<Estado> obtenerEstados() {
		return new ArrayList<Estado>() {{
			new AnuladoNoEjecutado();
			new CanceladoPorMantenimiento();
			new ConReservaConfirmada();
			new Disponible();
			new EnUso();
			new Finalizado();
			new PendienteDeConfirmacion();
		}};
	}
}
