package com.ppai.rescursostecologicos.config;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ppai.rescursostecologicos.dominio.AsignacionCientifico;
import com.ppai.rescursostecologicos.dominio.CambioEstadoRT;
import com.ppai.rescursostecologicos.dominio.CambioEstadoTurno;
import com.ppai.rescursostecologicos.dominio.CentroInvestigacion;
import com.ppai.rescursostecologicos.dominio.Cientifico;
import com.ppai.rescursostecologicos.dominio.Estado;
import com.ppai.rescursostecologicos.dominio.Marca;
import com.ppai.rescursostecologicos.dominio.Modelo;
import com.ppai.rescursostecologicos.dominio.RecursoTecnologico;
import com.ppai.rescursostecologicos.dominio.Sesion;
import com.ppai.rescursostecologicos.dominio.TipoRecursoTecnologico;
import com.ppai.rescursostecologicos.dominio.Turno;
import com.ppai.rescursostecologicos.dominio.Usuario;
import com.ppai.rescursostecologicos.dominio.estados.rt.Reservable;
import com.ppai.rescursostecologicos.dominio.estados.turno.Disponible;

@Configuration
public class DatosPrecargados {
	List<TipoRecursoTecnologico> tipos = new ArrayList<>();
	List<RecursoTecnologico> recursos = new ArrayList<>();
	
	DatosPrecargados() {		
		tipos.add(new TipoRecursoTecnologico(1, "Balanza de Presicion"));
		tipos.add(new TipoRecursoTecnologico(2, "taller"));
	
		List<AsignacionCientifico> asignaciones = new ArrayList<>();
		asignaciones.add(new AsignacionCientifico(new Cientifico(81704, "Lorenzo Perez", "lperez@centro.com")));
		
		CentroInvestigacion ci = new CentroInvestigacion("centro1", asignaciones);
		
		Marca marca = new Marca("Lenovo", new Modelo("Z4713"));
		Marca marca2 = new Marca("Lenovo", new Modelo("Thinkpad"));
		
		List<CambioEstadoRT> cambiosDeEstado = new ArrayList<>();
		cambiosDeEstado.add(new CambioEstadoRT(new Reservable(), LocalDate.now(), null));
		
		List<CambioEstadoTurno> cambiosDeEstadoTurno1 = new ArrayList<>();
		cambiosDeEstadoTurno1.add(new CambioEstadoTurno(new Disponible(), LocalDate.of(2022, 11, 4), null));
		List<CambioEstadoTurno> cambiosDeEstadoTurno2 = new ArrayList<>();
		cambiosDeEstadoTurno2.add(new CambioEstadoTurno(new Disponible(), LocalDate.of(2022, 11, 4), null));
		
		List<Turno> misTurnos = new ArrayList<>();
		Turno turno1 = new Turno(1, LocalDateTime.of(2022, 11, 4, 9, 30), LocalDateTime.of(2022, 11, 4, 10, 0), "Viernes", LocalDate.now(), LocalDate.of(2022, 11, 4), cambiosDeEstadoTurno1);
		Turno turno2 = new Turno(2, LocalDateTime.of(2022, 11, 4, 10, 00), LocalDateTime.of(2022, 11, 4, 10, 30), "Viernes", LocalDate.now(), LocalDate.of(2022, 11, 4), cambiosDeEstadoTurno2);
		misTurnos.add(turno1);
		misTurnos.add(turno2);
		
		recursos.add(new RecursoTecnologico(8253, ci, marca, tipos.get(0), cambiosDeEstado, misTurnos));
		recursos.add(new RecursoTecnologico(8254, ci, marca2, tipos.get(0), cambiosDeEstado, new ArrayList<>()));
	}
	
	@Bean
	public List<TipoRecursoTecnologico> tiposRecursoTecnologicos() {
		return tipos;
	}
	
	@Bean
	public List<RecursoTecnologico> recursos() {
		return recursos;
	}
	
	@Bean
	public Sesion obtenerSesionActual() {
		var cientifico = new Cientifico(52127, "Lorenzo Perez", "test@localhost");
		var usuario = new Usuario("user", "password", cientifico);
		return new Sesion(usuario);
	}
}
